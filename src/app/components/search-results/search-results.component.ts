import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SearchService } from '../../search.service';
import { Observable } from "rxjs/Rx"

import { ModalWindowComponent } from "./modal-window/modal-window.component";
import { isEmpty } from 'rxjs/operator/isEmpty';

@Component({
  selector: 'search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent {

  public data: any = false;
  public dataId: any = '';

  public currentVideo: any = false;
  public keyword: string = '';
  
  // Emmitter to ModalComponent
  @Output() modalEvent = new EventEmitter<string>();

  constructor(private _searchService: SearchService) {}
  
  // Request search
  async onYouTubeSearch() {
    const results = await this._searchService.searchYouTube(this.keyword).then((res)=> {
      return res;
    });

    this.dataId = results.etag;
    this.data = results.items;
  }
  
  openModal(event){
    const id = event.currentTarget.getAttribute('id');
    const video = this.data.find(video => (video.id.videoId === id));
    
    this.currentVideo = {
      id: id,
      title: video.snippet.title,
      thumbnail: video.snippet.thumbnails.medium.url,
    }
    this.sendVideoData();
  }
  
  // Send data to AppComponent
  sendVideoData() {
    this.modalEvent.emit(this.currentVideo);
  }

}
