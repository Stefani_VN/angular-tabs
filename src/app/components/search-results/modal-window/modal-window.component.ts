import { Component, Input } from '@angular/core';

@Component({
  selector: 'modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.scss']
})
export class ModalWindowComponent {

  constructor() { }
  
  // Bind results data from AppComponent
  @Input() videoData: any;

  
}
