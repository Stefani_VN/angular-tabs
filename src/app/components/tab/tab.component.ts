import { Component, Input, OnInit } from '@angular/core';
import { SearchService } from '../../search.service';
import { Observable } from "rxjs/Rx"

import { SearchResultsComponent } from '../search-results/search-results.component';

@Component({
  selector: 'tab',
  styles: [`
    .pane{
      padding: 1em;
    }
  `],
  templateUrl: './tab.component.html',
})
export class TabComponent {
  @Input('tabTitle') title: string;
  @Input() active = false;
  @Input() isCloseable = false;
  @Input() template;
  @Input() dataContext;

  data:string;

  constructor(private _searchService: SearchService) {}

}