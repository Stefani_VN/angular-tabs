
import { Component, ContentChildren, QueryList, AfterContentInit, ViewChild, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';

import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements AfterContentInit {
  dynamicTabs: TabComponent[] = [];
  
  @ContentChildren(TabComponent) 
  tabs: QueryList<TabComponent>;
  
  @ViewChild('container', {read: ViewContainerRef}) dynamicTabPlaceholder;
  
  constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}
  
  ngAfterContentInit() {
    // get all active tabs
    let activeTabs = this.tabs.filter((tab)=>tab.active);
    
    // if there is no active tab set, activate the first
    if(activeTabs.length === 0) {
      this.selectTab(this.tabs.first);
    }
  }
  
  openTab(title: string, template, data, isCloseable = false) {
    // get a component factory for TabComponent
    let componentFactory = this._componentFactoryResolver.resolveComponentFactory(TabComponent);
    
    let viewContainerRef = this.dynamicTabPlaceholder;
    
    // create a component instance
    let componentRef = viewContainerRef.createComponent(componentFactory);

    // set the according properties on our component instance
    let instance: TabComponent = componentRef.instance as TabComponent;
    instance.title = title;
    instance.template = template;
    instance.dataContext = data;
    instance.isCloseable = isCloseable;
    
    this.dynamicTabs.push(componentRef.instance as TabComponent);
    
    // set it active
    this.selectTab(this.dynamicTabs[this.dynamicTabs.length - 1]);
  }
  
  selectTab(tab: TabComponent){
    // deactivate all tabs
    this.tabs.toArray().forEach(tab => tab.active = false);
    this.dynamicTabs.forEach(tab => tab.active = false);
    
    // activate the tab the user has clicked on.
    tab.active = true;
  }
  
  closeTab(tab: TabComponent) {
    for(let i=0; i<this.dynamicTabs.length;i++) {
      if(this.dynamicTabs[i] === tab) {

        this.dynamicTabs.splice(i,1);
        
        let viewContainerRef = this.dynamicTabPlaceholder;
        viewContainerRef.remove(i);
        
        // set tab index to 1st one
        this.selectTab(this.tabs.first);
        break;
      }
    }
  }
  
  closeActiveTab() {
    let activeTabs = this.dynamicTabs.filter((tab)=>tab.active);
    if(activeTabs.length > 0)  {
      this.closeTab(activeTabs[0]);
    }
  }

}
