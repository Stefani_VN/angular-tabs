import { BrowserModule, VERSION } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TabComponent } from './components/tab/tab.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { GoogleApiModule,
         GoogleApiService,
         NgGapiClientConfig,
         NG_GAPI_CONFIG,
         GoogleApiConfig } from 'ng-gapi';
import { SearchService } from './search.service';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { ModalWindowComponent } from './components/search-results/modal-window/modal-window.component';

let gapiClientConfig: NgGapiClientConfig = {
    client_id: "AIzaSyBBRwqVh89XXlTZK_cF0P9BdLgFrNRYHF0",
    discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest"]  
};

@NgModule({
  declarations: [
    AppComponent,
    TabComponent,
    TabsComponent,
    SearchResultsComponent,
    ModalWindowComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: gapiClientConfig
    })
  ],
  providers: [SearchService],
  bootstrap: [AppComponent],
  entryComponents: [TabComponent]
})
export class AppModule { }
