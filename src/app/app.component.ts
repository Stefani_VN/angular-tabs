import { Component, ViewChild, AfterViewInit } from '@angular/core';

import { TabsComponent } from './components/tabs/tabs.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  // Communicate with children components
  
  @ViewChild('searchResults') searchTemplate; // Anchor point
  @ViewChild(TabsComponent) tabsComponent;

  public currentVideo; 
  
  // Bind results data
  getData($event) {
    this.currentVideo = $event;
  }

  // Create new tab
  onOpenNewTab() {
    this.tabsComponent.openTab(
      'Search',
      this.searchTemplate, 
      {},
      true
    );
  }

}
