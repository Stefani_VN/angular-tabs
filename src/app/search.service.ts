import { Injectable, Output, EventEmitter } from '@angular/core';
import { GoogleApiService } from 'ng-gapi';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http, Response } from "@angular/http"


@Injectable()
export class SearchService {

  private youtubeApi: any;
  searchResult: any;
  
  private dataSource = new BehaviorSubject<string>("default message");
  currentData = this.dataSource.asObservable();

  constructor(private gapiService: GoogleApiService) { 
    let _this = this;
    // Load the YouTube API
    gapiService.onLoad().subscribe(data => {
      gapi.load('client', ()=> {
        gapi.client.init({
          'apiKey': 'AIzaSyBBRwqVh89XXlTZK_cF0P9BdLgFrNRYHF0',
          'discoveryDocs': ["https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest"]
        })
      });
    });
  }
  
  sendResults(data: any) {
    this.dataSource.next(data);
  }
  
  // Make request and execute to return results
  async searchYouTube(keyword) {
      this.searchResult = await gapi['client']['youtube']['search'].list({
        q: keyword,
        part: 'snippet',
        maxResults: 30
      }).then(function(response) {
        return response.result;
      });
      return this.searchResult;
  }

}
